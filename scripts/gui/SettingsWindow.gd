extends WindowDialog

#On change, new values to UserSettings singleton, which calls the AudioPlayerNode groups


#Store interactive nodes for easy access
onready var master_slider = $MarginContainer/VBoxContainer/MasterContainer/MasterSlider
onready var music_slider = $MarginContainer/VBoxContainer/MusicContainer/MusicSlider
onready var sound_slider = $MarginContainer/VBoxContainer/SoundContainer/SoundSlider

onready var muaster_checkbox = $MarginContainer/VBoxContainer/MasterContainer/MasterCheck
onready var music_checkbox = $MarginContainer/VBoxContainer/MusicContainer/MusicCheck
onready var sound_checkbox = $MarginContainer/VBoxContainer/SoundContainer/SoundCheck

#load values from UserSettings to display them
func _on_SettingsWindow_about_to_show():
	master_slider.value = UserSettings.master_volume
	music_slider.value = UserSettings.music_volume
	sound_slider.value = UserSettings.sound_volume
	
#pass changes to UserSettings on signal
func _on_MasterSlider_value_changed(value):
	UserSettings.master_volume = value

func _on_MusicSlider_value_changed(value):
	UserSettings.music_volume = value

func _on_SoundSlider_value_changed(value):
	UserSettings.sound_volume = value
	
func _on_MasterCheck_toggled(button_pressed):
	UserSettings.master_muted = button_pressed	

func _on_MusicCheck_toggled(button_pressed):
	UserSettings.music_muted = button_pressed


func _on_SoundCheck_toggled(button_pressed):
	UserSettings.sound_muted = button_pressed
	
func _on_BackButton_pressed():
	hide()

