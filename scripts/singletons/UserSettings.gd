extends Node

#indize all created audio busses for access in code
enum BUSIDX { MASTER, MUSIC, SOUND }

# Default Sound Configuration when the game starts
var master_volume = 0.5 setget set_master_volume
var music_volume = 0.5 setget set_music_volume
var sound_volume = 0.5 setget set_sound_volume

var master_muted = false setget set_master_muted
var music_muted = false setget set_music_muted
var sound_muted = false setget set_sound_muted

func _ready():	
	AudioServer.set_bus_mute(MASTER,master_muted)
	AudioServer.set_bus_mute(MUSIC,music_muted)
	AudioServer.set_bus_mute(SOUND,sound_muted)
	
	AudioServer.set_bus_volume_db(MASTER,linear2db(master_volume))
	AudioServer.set_bus_volume_db(MUSIC,linear2db(music_volume))
	AudioServer.set_bus_volume_db(SOUND,linear2db(sound_volume))
	
func set_master_volume(value):
	master_volume = value
	AudioServer.set_bus_volume_db(MASTER,linear2db(value))

func set_master_muted(value):
	sound_muted = value
	AudioServer.set_bus_mute(MASTER,value)

func set_music_volume(value):
	music_volume = value
	AudioServer.set_bus_volume_db(MUSIC,linear2db(value))

func set_music_muted(value):
	music_muted = value
	AudioServer.set_bus_mute(MUSIC,value)

func set_sound_volume(value):
	sound_volume = value
	AudioServer.set_bus_volume_db(SOUND,linear2db(value))

func set_sound_muted(value):
	sound_muted = value
	AudioServer.set_bus_mute(SOUND,value)