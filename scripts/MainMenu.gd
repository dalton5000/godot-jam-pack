extends Control

#

func _on_SettingsButton_pressed():
	$SettingsWindow.popup()

func _on_Button_pressed():
	$Button/TESTSFX.play()

func _on_StartButton_pressed():
	get_tree().change_scene_to(load("res://scenes/Game.tscn"))

func _on_QuitButton_pressed():
	get_tree().quit()
